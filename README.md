# Телеграмм бот по оповещению о MR

Телеграмм бот по оповещению о MR с Dockerfile и k8s манифестами


### Отладка

Для локальной отладки нужно объявить переменные:
```bash
export GL_TOKEN="Bearer <enter your token>"
export TG_TOKEN='<enter your token>'
```
### Запуск в Docker

Для запуска в Docker:
```bash
docker build . -t mr:v0.1
docker run mr:v0.1
```

### Запуск в Kubernetes

Для запуска в Kubernetes:
```bash
## Тегаем и пушим образ в Dockerhub 
docker tag mr:v0.1 tatita/mrbot:v0.1
docker push tatita/mrbot:v0.1
## Заполняем k8s/secretCopy.yaml и применяем все манифесты в кластер
kubectl apply -f k8s/
```